# APIs
## Photos (external): https://www.pexels.com/api/documentation/#introduction
## Weather API (external): https://openweathermap.org/guide
## Attendees
* **Method**: `GET, POST`
* **Path**: /api/attendees/
Input:
```json
{
  "email": string,
  "name": string,
  "conpany_name": string,
  "conference": integer
}
```
Output:
```json
{
  "name": string,
}
```
- **Method**: `GET`
- **Path**: /api/attendees/conference_id/
Output:
```json
{
  "name": string,
}
```
- **Method**: `GET, PUT, DELETE`
- **Path**: /api/attendees/attendee_id/
Input:
```json
{
  "email": string,
  "name": string,
  "conpany_name": string,
  "conference": integer
}
```
Output:
```json
{
  "name": string,
}
```
## Accounts
* **Method**: `GET, POST`
* **Path**: /api/accounts/
Input:
```json
{
  "email": string,
  "first_name": string,
  "last_name": string,
}
```
Output:
```json
{
  "email": string,
  "first_name": string,
  "last_name": string,
}
```
- **Method**: `GET`
- **Path**: /api/accounts/email_string/
Output:
```json
{
  "email": string,
  "first_name": string,
  "last_name": string,
  "is_active": boolean
}
```
## Events
* **Method**: `GET, POST`
* **Path**: /api/conferences/
Input:
```json
{
  "name": string,
  "description": string,
  "nax_attendees": integer,
  "starts": iso datetime string,
  "ends": iso datetime string,
  "location": integer,
}
```
Output:
```json
{
  "name": string,
  "id": integer
}
```
* **Method**: `GET, PUT, DELETE`
* **Path**: /api/conferences/conference_id/
Input:
```json
{
  "name": string,
  "description": string,
  "nax_attendees": integer,
  "starts": iso datetime string,
  "ends": iso datetime string,
  "location": integer,
}
```
Output:
```json
{
  "name": string,
  "description": string,
  "nax_attendees": integer,
  "starts": iso datetime string,
  "ends": iso datetime string,
  "created": iso datetime string,
  "updated": iso datetime string,
  "location": integer,
}
```
* **Method**: `GET, POST`
* **Path**: /api/locations/
Input:
```json
{
  "name": string,
  "city": string,
  "state": integer,
  "room_count": integer
}
```
Output:
```json
{
  "name": string,
  "picture_url": string,
  "id": integer
}
```
* **Method**: `GET, PUT, DELETE`
* **Path**: /api/locations/location_id/
Input:
```json
{
  "name": string,
  "city": string,
  "state": integer,
  "room_count": integer
}
```
Output:
```json
{
  "name": string,
  "city": string,
  "state": string,
  "picture_url": string,
  "room_count": integer,
  "created": iso datetime string,
  "updated": iso datetime string,
  "id": integer
}
```
* **Method**: `GET`
* **Path**: /api/states/
Output:
```json
{
  "name" string,
  "abbreviation": string
}
```
## Events
* **Method**: `GET, POST`
* **Path**: /api/conferences/conference_id/presentations/
Input:
```json
{
  "presenter_name": string,
  "company_name": string,
  "presenter_email": string,
  "title": string,
  "synopsis": string,
  "conference": integer
}
```
Output:
```json
{
  "title": string,
  "status": string
}
```
* **Method**: `GET, PUT, DELETE`
* **Path**: /api/presentations/presentation_id/
Input:
```json
{
  "status": integer
}
```
Output:
```json
{
  "presenter_name": string,
  "company_name": string,
  "presenter_email": string,
  "title": string,
  "synopsis": string,
  "conference": integer,
  "created": iso datetime string,
  "status": string
}
```
