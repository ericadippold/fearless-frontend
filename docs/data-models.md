# Data models
All schema for databases created for this project can be referenced here.

## Monolith (Django)

### Accounts App: User
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| email | EmailField | yes | no |

### Events App: Conference
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| name | CharField | no | no |
| description | CharField | no | no |
| starts | DateTime | no | no |
| ends | DateTime | no | no |
| created | DateTime | no | no |
| updated | DateTime | no | no |
| location | ForeignKey to Location | no | no |

### Events App: Location
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| name | CharField | no | no |
| city | CharField | no | no |
| room_count | PositiveSmallIntegerField | no | no |
| created | DateTime | no | no |
| updated | DateTime | no | no |
| picture_url | URLField | no | no |
| state | ForeignKey to State | no | no |

### Events App: State
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| name | CharField | yes | no |
| abbreviation | CharField | yes | no |

### Presentations App: Presentation
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| presenter_name | CharField | no | no |
| company_name | CharField | no | no |
| title | CharField | no | no |
| synopsis | CharField | no | no |
| created | DateTime | no | no |
| status | ForeignKey to Status | no | no |

### Presentation App: Status
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| name | CharField | yes | no |

## Attendees Microservice (Django)

### Attendee
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| email | EmailField | yes | no |
| name | CharField | no | no |
| company_name | CharField | no | yes |
| created | DateTime | no | no |
| conference | ForeignKey to ConferenceVO | no | no |

### AccountVO
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| email | EmailField | yes | no |
| first_name | CharField | no | no |
| first_name | CharField | no | no |
| updated | DateTime | no | no |

### ConferenceVO
| Name | Type | Unique | Optional |
|-|-|-|-|
| id | serial | yes | no |
| import_href | CharField | yes | no |
| name | CharField | no | no |
