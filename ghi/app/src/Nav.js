import './index.css';
import * as React from "react";
import { NavLink } from "react-router-dom";


function Nav() {
return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Conference GO!</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new-location" aria-current="page" to="/locations/new">New location</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new-conference" aria-current="page" to="/conferences/new">New conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" id="new-presentation" to="/presentations/new">New presentation</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    );
}

export default Nav;

//we have to pull the data from here and pass it to the
// app.js file
//as we did in the index.js file


//the index.js file allowed us to get our attendees
// info and pass it over to App.js
